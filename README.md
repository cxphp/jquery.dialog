# jquery.dialog [![spm version](http://spmjs.io/badge/jquery.dialog)](http://spmjs.io/package/jquery.dialog)

---

基于jQuery弹出层插件

## Install

```
$ spm install jquery.dialog --save
```

## Usage

```js
var dialog = require('jquery.dialog');
// use jquery.dialog
```
